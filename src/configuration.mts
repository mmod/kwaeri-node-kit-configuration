/**
 * SPDX-PackageName: kwaeri/configuration
 * SPDX-PackageVersion: 0.6.0
 * SPDX-FileCopyrightText: © 2014 - 2022 Richard Winters <kirvedx@gmail.com> and contributors
 * SPDX-License-Identifier: Apache-2.0 WITH LLVM-exception OR MIT
 */


'use strict'


// INCLUDES
import * as _fs from 'node:fs/promises'
import * as _path from 'path';
import { Filesystem, FilesystemEntry, FilesystemRecord } from '@kwaeri/filesystem';
import debug from 'debug';


// DEFINES
const DEBUG = debug( 'kue:configuration' );


/**
 * The Configuration Class
 *
 * The { Configuration } class exists to read and to save a configuration file
 * which already exists. It can also generate a configuration file on the fly
 * that must be populated manually (it would otherwise be generated and
 * managed by the project generator).
 */
export class Configuration extends Filesystem {
    /**
     * @var { string }
     */
    path: string = "";


    /**
     * @var { string }
     */
    file: string = "";


    /**
     * @var { boolean }
     */
    plainText: boolean = false;


    /**
     * @var { string }
     */
    type: string = "";

    /**
     * @var { boolean }
     */
    tryDefaultOnFail = true;


    /**
     * @var { any  }
     */
    configuration: any = null;


    /**
     * Class constructor
     */
    //constructor( path?: string, file?: string, plainText: boolean = false, type?: string, tryDefaultOnFail?: boolean ) {
    //    super();

    //    // Set some default values:
    //    this.path = Filesystem.SyntaxError: Unexpected token p in JSON at position 0getPathToCWD();

        // In case the path is provided:, otherwise append it to CWD
        //if( path && path !== "" )
        //    this.path += `/${path}`;

        // Ensure file is provided:
        //this.file = ( file && file !== "" ) ? file : 'kwaeri.default.json';

        // If no plainText flag is provided:
        //this.plainText = plainText || this.plainText;

        // If No type is provided, use the default (kwaeri):
        //this.type = ( type && SyntaxError: Unexpected token p in JSON at position 0type !== "" ) ? type : 'kwaeri';
    //}




    /**
     * Configuration class constructor
     *
     * @param { string } path The path to the configuration directory
     * @param { string } file The name of the configuration file. May include the
     *                       environment and/or the extention, in the form of
     *                       `name.env.ext` or `name.ext`.
     * @param { any } options An object containing the following optional values:
     *
     * * `plainText` (boolean): Whether the configuration file is plain text or
     * JSON. Defaults to `false`.
     * * `type` (string): A historic property which is now useful mostly for the
     *  debugging of configutration files and this configuration utility. It can
     *  be any string value that you'd like (e.g. `kwaeri`, `kue`, `kue-cli`, etc.)
     * and will be used in any DEBUG output. Defaults to `kwaeri`.
     * * `tryDefaultOnFail` (boolean): Whether to attempt to read a default variant
     * of the configuration file if the requested configuration file does not exist.
     */
    constructor( path?: string, file?: string,options: any =  { plainText: false, type: null, tryDefaultOnFail: true } ) {
        super();

        // Set some default values:
        this.path = Filesystem.getPathToCWD();

        // In case the path is provided:, otherwise append it to CWD
        if( path && path !== "" )
            this.path += `/${path}`;

        // Ensure file is provided:
        this.file = ( file && file !== "" ) ? file : 'kwaeri';

        // If no plainText flag is provided:
        this.plainText = options.plainText || this.plainText;

        // If No type is provided, use the default (kwaeri):
        this.type = ( options.type && options.type !== "" ) ? options.type : 'kwaeri';

        // If tryDefaultOnFail is true we will check for a "default" configuration
        // assuming the requested configuration does not exist: We do not want to
        // set this to false for null or undefined, as we want to allow the user
        // to explicitly set it to false, with it otherwise being true by default:
        this.tryDefaultOnFail = ( options.tryDefaultOnFail || options.tryDefaultOnFail === false ) ? options.tryDefaultOnFail : this.tryDefaultOnFail;
    }

    /**'
     * @returns the assumed extension of the configuration file
     */
    get assumedExt(): string { return ( this.plainText ) ? 'conf' : 'json'; }

    /**
     * @returns The current NODE_ENV value, or "default" if not set
     */
    get assumedEnv(): string { return ( process.env.NODE_ENV && process.env.NODE_ENV !== "" )? process.env.NODE_ENV : 'default'; }

    /**
     * @returns { boolean } Whether the file name contains an extension (assumed
     *                      by presence of multiple parts separated by a period).
     */
    get fileHasExtension(): boolean {
        const fileParts = this.file.split( '.' );
        return ( fileParts.length > 1 && fileParts.length <= 3 );
    }

    /**
     * Reads a Configuration from disk
     *
     * @param { void }
     *
     * @returns { Promise<any> } a promise of a JSON object or `String`
     */
    async read(): Promise<any> {
        try {
            // Let's first make sure that the filename contains the extension,
            // and if not then let's tack it on!
            let filename = ( this.fileHasExtension )?this.file:`${this.file}.${this.assumedEnv}.${this.assumedExt}`;
            let fullPath = `${this.path}/${filename}`;
            //let fullPath = `${this.path}/${this.file}`;

            DEBUG( `Check that the configuration directory '${this.path}' and file '${filename} exists` );

            // First thing to do is to check whether the conf directory does exist:
            if( !( await this.exists( this.path ) ) || !( await this.exists( fullPath ) ) ) {

                DEBUG( `Provided configuration not found in the current [${process.env.NODE_ENV}]: '${fullPath}' environment: '${fullPath}'` );

                // If we are not able to find the configuration requested, and
                // we are not allowed to check for a default, fail:
                if( !this.tryDefaultOnFail  )
                    return Promise.resolve( false );

                DEBUG( `Trying default configuration` );

                // Otherwise, let's see if a "default" variant of the provided
                // configuration exists
                let targetFile = "";
                if( this.fileHasExtension ) {
                    // If the provided file name included an extension, add or
                    // replace the environment component
                    const fileParts = this.file.split( '.' );
                    targetFile =  `${ fileParts[0]}.default.${fileParts[( fileParts.length-1 )]}`;
                }
                else {
                    // Otherwise, provide the default environment and extension
                    targetFile = `${this.file}.default.${this.assumedExt}`;
                }

                if( await !this.exists( this.path ) || await !this.exists( `${this.path}/${targetFile}` ) ) {
                    // If in this case we fail to find the target configuration
                    // return
                    DEBUG( `Provided configuration not found in the current [${process.env.NODE_ENV}]: '${fullPath}', nor the [default] environment: '${this.path}/${targetFile}'` );

                    return Promise.resolve( false );
                }
                else {
                    // If we found the "default" configuration, lets fix the
                    // fullPath value
                    DEBUG( `Provided configuration found, in the [default] environment: '${this.path}/${targetFile}'` );

                    fullPath = `${this.path}/${targetFile}`;
                }
            }
            else {
                DEBUG( `Provided configuration found in the current [${process.env.NODE_ENV}] environment: '${fullPath}'` );
            }

            DEBUG( `Read configuration '${fullPath}'` );

            const data = await _fs.readFile( fullPath, { encoding: "utf8"} );

            this.configuration = ( !this.plainText ) ? JSON.parse( data as any ) : data;

            DEBUG( this.configuration );

            return Promise.resolve( this.configuration );

        }
        catch( exception ) {
            DEBUG( exception );

            return Promise.reject( exception );
        }
    }


    /**
     * Write a Configuration to disk
     *
     * @param { any } configuration The object or plain text configuration to write to disk
     *
     * @returns { Promise<boolean> } a promise of a `boolean`
     */
    async write( configuration?: any ): Promise<boolean> {
        try {
            DEBUG( `Write configuration (%s, environment: %s, type: %s) `, this.assumedEnv, this.assumedExt );

            // Write the configuration file
            //
            // First figure out if we need to specify the whole file name:
            //const filenameHasExtension = ( ( this.file.split( '.' ) ).length > 1 );
            const wroteConfigurationFile = await this.createFile(
                                                this.path,
                                                ( this.fileHasExtension ) ? this.file : `${this.file}.${this.assumedEnv}.${this.assumedExt}`,
                                                ( !this.plainText ) ?
                                                ( ( configuration ) ? JSON.stringify( configuration ) : JSON.stringify( this.configuration ) ) :
                                                ( ( configuration ) ? configuration : this.configuration ) );

            // Return that we've succeeded and the configuration that was written:
            return Promise.resolve( wroteConfigurationFile );
        }
        catch( exception ) {
            // Reject the promise:
            return Promise.reject( exception );
        }
    }


    /**
     * Returns an already read configuration, or an attempt to read it.
     *
     * @param { void }
     *
     * @returns { Promise<any> } a promise of a JSON object or `String`
     */
    async get(): Promise<any> {
        DEBUG( `Get configuration` );

        // If the configuration has already been read, return it:
        if( this.configuration ) {
            DEBUG( `Get configuration previously read from '${this.path}/${this.file}':` );
            DEBUG( this.configuration );

            return Promise.resolve( this.configuration );
        }

        // Otherwise attempt to read it:
        try {
            DEBUG( `Configuration '${this.path}/${this.file}' not already read in.` );

            // If the configuration does exist, it will return true - otherwise false
            return Promise.resolve( await this.read() );
        }
        catch( exception ) {
            DEBUG( exception );

            // We'll only reject this method if there is an error:
            return Promise.reject( exception );
        }
    }
}

