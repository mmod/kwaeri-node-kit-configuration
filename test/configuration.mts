/**
 * SPDX-PackageName: kwaeri/configuration
 * SPDX-PackageVersion: 0.6.0
 * SPDX-FileCopyrightText: © 2014 - 2022 Richard Winters <kirvedx@gmail.com> and contributors
 * SPDX-License-Identifier: Apache-2.0 WITH LLVM-exception OR MIT
 */


'use strict'

// INCLUDES
import * as assert from 'assert';
import debug from 'debug';
import path from 'path';
import { Configuration } from '../src/configuration.mjs';


// DEFINES
const DEBUG = debug( `kue:configuration-test` );

const configuration = new Configuration( "data", "test", { plainText: false, type: ""} ),
      plainTextConfiguration = new Configuration( "data", "plaintext", { plainText: true, type: "plaintext", tryDefaultOnFail: false } ),
      badJsonConfiguration = new Configuration( "data", "bad", { plainText: false, type: "badjson" } ),
      fakeConfiguration = new Configuration();


// SANITY CHECK - Makes sure our tests are working proerly
describe(
    'PREREQUISITE',
    () => {
        describe(
            'Sanity Test(s)',
            () => {

                it(
                    'Should return true.',
                    () => {
                        assert.equal( [1,2,3,4].indexOf(4), 3 );
                    }
                );

            }
        );
    }
);


describe(
    'Configuration Functionality Test Suite',
    () => {


        describe(
            'Read Configuration Test [I. Non-existant]',
            () => {
                it(
                    `Should fail to read non-existant files: \n          [default path]: "../../data/"`,
                    async () => {
                        try {
                            const read = await fakeConfiguration.read();

                            return Promise.resolve(
                                assert.equal(
                                    read,
                                    false
                                )
                            );
                        }
                        catch( exception ) {
                            DEBUG( exception );
                        }
                    }
                );
            }
        );


        describe(
            'Read Configuration Test [II. Plain-text]',
            () => {
                it(
                    `Should read plain text files: \n          [path]: "../../data/plaintext.default.conf"`,
                    async () => {
                        try {
                            return Promise.resolve(
                                assert.equal(
                                    await plainTextConfiguration.read(),
                                    "param   value\nparam2  value2"
                                )
                            );
                        }
                        catch( exception ) {
                            DEBUG( exception );
                        }
                    }
                );
            }
        );


        describe(
            'Read Configuration Test [III. Default - Proper JSON]',
            () => {
                it(
                    `Should read and parse properly formed JSON files: \n          [path]: "../../data/test.default.json"`,
                    async () => {
                        try {
                            return Promise.resolve(
                                assert.equal(
                                    JSON.stringify( await configuration.read() ),
                                    JSON.stringify(
                                        {
                                            "first": "test",
                                            "second": [
                                                "third",
                                                "fourth",
                                                "fifth"
                                            ]
                                        }
                                    )
                                )
                            );
                        }
                        catch( exception ) {
                            DEBUG( exception );
                        }
                    }
                );
            }
        );


        describe(
            'Read Configuration Test [IV. Improper JSON]',
            () => {
                it(
                    `Should fail to parse json in json mode: \n          [read plain text configuration]: "../../data/bad.default.json".`,
                    async () => {
                        try {
                            const result = await badJsonConfiguration.read();
                        }
                        catch( exception ) {
                            DEBUG( exception );

                            return Promise.resolve(
                                assert.equal(
                                    `${( exception as Error ).name}: ${( exception as Error ).message}`,
                                    `SyntaxError: Unexpected token 'p', "param   va"... is not valid JSON`
                                )
                            );
                        }
                    }
                );
            }
        );


        describe(
            'Get Configuration Test [I. Non-existant]',
            () => {
                it(
                    `Should fail to return non-existant files: \n          [default path]: "../../data/"`,
                    async () => {
                        try {
                            return Promise.resolve(
                                assert.equal(
                                    await fakeConfiguration.get(),
                                    false
                                )
                            );
                        }
                        catch( exception ) {
                            DEBUG( exception );
                        }
                    }
                );
            }
        );


        describe(
            'Get Configuration Test [II. Plain-text, already read-in]',
            () => {
                it(
                    `Should return already read-in files: \n          [plaintext, path]: "../../plaintext.default.conf"`,
                    async () => {
                        try {
                            return Promise.resolve(
                                assert.equal(
                                    await plainTextConfiguration.get(),
                                    "param   value\nparam2  value2"
                                )
                            );
                        }
                        catch( exception ) {
                            DEBUG( exception );
                        }
                    }
                );
            }
        );


        describe(
            'Get Configuration Test [III. JSON, not yet read-in]',
            () => {
                it(
                    `Should cause and return a read-in file \n          [json, path]: "../../data/test.default.conf".`,
                    async () => {
                        try {
                            const conf = new Configuration( 'data', 'test', { plainText: false, type: 'test' } );
                            return Promise.resolve(
                                assert.equal(
                                    JSON.stringify( await conf.get() ),
                                    JSON.stringify(
                                        {
                                            "first": "test",
                                            "second": [
                                                "third",
                                                "fourth",
                                                "fifth"
                                            ]
                                        }
                                    )
                                )
                            );
                        }
                        catch( exception ) {
                            DEBUG( exception );
                        }
                    }
                );
            }
        );


        describe(
            'Write Configuration Test [I. Default, already read-in, JSON]',
            () => {
                it(
                    `Should write its file \n          [default json, path]: "../../data/test.default.json".`,
                    async () => {
                        try {
                            // First read
                            let original = await configuration.get() as any;

                            // Then modify
                            original.second = ["test", "write", "conf" ];
                            const modified = JSON.parse( JSON.stringify( original ) )

                            // Then write
                            const write = await configuration.write( modified );
                            const read = await configuration.read() as any;

                            // Then fix
                            original.second = ["third", "fourth", "fifth" ];
                            const fixed = JSON.parse( JSON.stringify( original ) );
                            const rewrite = await configuration.write( fixed );

                            return Promise.resolve(
                                assert.equal(
                                    JSON.stringify( read ),
                                    JSON.stringify( modified )
                                )
                            );
                        }
                        catch( exception ) {
                            DEBUG( exception );
                        }
                    }
                );
            }
        );


        describe(
            'Write Configuration Test [II. Existant, already read-in, plain-text]',
            () => {
                it(
                    `Should write its file: \n          [paintext, path]: "../../data/plaintext.default.json".`,
                    async () => {
                        try {
                            // First read
                            const original = await plainTextConfiguration.get();

                            // Then modify
                            const modified = original + "\nparam3  value3";

                            // Then write
                            const write = await plainTextConfiguration.write( modified );
                            const read = await plainTextConfiguration.read();

                            // Then fix
                            const rewrite = await plainTextConfiguration.write( original );

                            return Promise.resolve(
                                assert.equal(
                                    read,
                                    modified
                                )
                            );
                        }
                        catch( exception ) {
                            DEBUG( exception );
                        }
                    }
                );
            }
        );


        describe(
            'Write Configuration Test [III. Non-existant, not read-in, new, plain-text]',
            () => {
                it(
                    `Should create a new file: \n          [plaintext, path]: "../../data/write.default.json".`,
                    async () => {
                        try {
                            // First read
                            const conf = new Configuration( 'data', 'write', { plainText: true, type: 'test', tryDefaultOnFail: false } );

                            // Then modify
                            const original = '.';

                            const write = await conf.write( original );
                            const read = await conf.get();

                            // Then remove the file for future tests
                            const removed = await conf.remove( path.join( Configuration.getPathToCWD(), '/data/write.default.conf' ) );

                            if( !removed )
                                return Promise.reject( new Error( `write.default.json could not be removed` ) );

                            return Promise.resolve(
                                assert.equal(
                                    read,
                                    original
                                )
                            );
                        }
                        catch( exception ) {
                            DEBUG( exception );
                        }
                    }
                );
            }
        );

    }
);
